weka (3.6.14-4) UNRELEASED; urgency=medium

  * d/p/set_compiler_release.patch, d/rules: use  java_compat_level
    variable provided by java-common to adjust -release level (Closes:
    #1053086).

 -- Vladimir Petko <vladimir.petko@canonical.com>  Mon, 04 Dec 2023 18:09:28 +1300

weka (3.6.14-3) unstable; urgency=medium

  * Team Upload.
  * Add d/salsa-ci.yml
  * d/p/reproducible.patch: Remove date from doc to
    make build reproducible (Closes: #986642)
  * Fix pdf docbase name

 -- Nilesh Patra <nilesh@debian.org>  Sun, 15 Aug 2021 16:44:20 +0530

weka (3.6.14-2) unstable; urgency=medium

  * Team upload.
  * Fixed the build failure with OpenJDK 17 (Closes: #981828)
  * Build with the DH sequencer instead of CDBS
  * Standards-Version updated to 4.5.1
  * Switch to debhelper level 13
  * Use salsa.debian.org Vcs-* URLs

 -- Emmanuel Bourg <ebourg@apache.org>  Mon, 08 Feb 2021 15:20:21 +0100

weka (3.6.14-1) unstable; urgency=medium

  * New upstream version.
  * Tweak weka-doc package description.
  * Bump Standards-Version to 3.9.8 (no changes).
  * Use HTTPS for Vcs URLs.

 -- tony mancill <tmancill@debian.org>  Sun, 07 Aug 2016 19:29:59 -0700

weka (3.6.13-1) unstable; urgency=medium

  * New upstream release.

 -- tony mancill <tmancill@debian.org>  Tue, 13 Oct 2015 22:15:21 -0700

weka (3.6.12-2) unstable; urgency=medium

  * Upload to unstable.

 -- tony mancill <tmancill@debian.org>  Sun, 26 Apr 2015 09:55:27 -0700

weka (3.6.12-1) experimental; urgency=medium

  * New upstream release.
  * Bump Standards-Version to 3.9.6 (no changes).

 -- tony mancill <tmancill@debian.org>  Sun, 15 Mar 2015 19:12:22 -0700

weka (3.6.11-1) unstable; urgency=medium

  * New upstream release.
  * Update wrapper script to work with any java6-runtime.

 -- tony mancill <tmancill@debian.org>  Sat, 03 May 2014 15:49:06 -0700

weka (3.6.10-2) unstable; urgency=low

  * Add libsvm-java to Suggests and to find_jars in weka launcher.
  * Update launcher to look for openjdk7 first.
  * Bump Standards-Version to 3.9.5 (no changes).

 -- tony mancill <tmancill@debian.org>  Wed, 04 Dec 2013 21:52:58 -0800

weka (3.6.10-1) unstable; urgency=low

  * New upstream release.
  * Bump Standards-Version to 3.9.4.
  * Update Vcs fields to use canoncal URLs.
  * Update debian/compat and debhelper version to 9.
  * Update Java runtime dependencies.

 -- tony mancill <tmancill@debian.org>  Mon, 02 Sep 2013 16:35:59 -0700

weka (3.6.6-1) unstable; urgency=low

  * Team upload.
  * New upstream version (Closes: #649734)
  * Update rules and build-deps for default-jdk and default-jre.

 -- tony mancill <tmancill@debian.org>  Thu, 24 Nov 2011 12:35:17 -0800

weka (3.6.5-1) unstable; urgency=low

  * Team upload.
  * New upstream version (Closes: #632082, #598400)
  * Bump Standards-Version to 3.9.2 (no changes required).
  * Freshen jar.patch; remove java_cup.patch (incorporated upstream)
  * Add README.source to document process of obtaining .png resources.
  * Add myself to Uploaders.

 -- tony mancill <tmancill@debian.org>  Fri, 05 Aug 2011 22:40:50 -0700

weka (3.6.0-3) unstable; urgency=low

  * Bump Standards-Version to 3.8.4 (no changes required).
  * Switch to dpkg-source 3.0 (quilt) format.
  * Track only stable releases in watch file.

 -- Soeren Sonnenburg <sonne@debian.org>  Fri, 23 Apr 2010 22:33:11 +0200

weka (3.6.0-2) unstable; urgency=low

  * Change Maintainer: Debian Java Maintainers.
  * Update Standards-Version: 3.8.2 (no changes).

 -- Torsten Werner <twerner@debian.org>  Mon, 29 Jun 2009 22:02:28 +0200

weka (3.6.0-1) unstable; urgency=low

  * New (stable) upstream version.
  * Update copyright and patches to reflect upstream changes.
  * Build single .pdf document.
  * Add ${misc:Depends} to weka-doc dependencies to fix lintian warning.

 -- Soeren Sonnenburg <sonne@debian.org>  Thu, 25 Dec 2008 11:53:48 +0100

weka (3.5.8+cup1-2) unstable; urgency=low

  * Add versioned cup to dependencies (Closes: #504897).
  * Explicitly require openjdk-6-jdk in build dependencies.
  * Require 256m to allow successful building of javadocs.

 -- Soeren Sonnenburg <sonne@debian.org>  Mon, 24 Nov 2008 10:06:45 +0100

weka (3.5.8+cup1-1) unstable; urgency=low

  [ Torsten Werner ]
  * Update Section field in doc-base file.
  * Add Class-Path attribute to jar file.

  [ Soeren Sonnenburg ]
  * Update my email address to sonne@debian.org.
  * Update copyright.
  * Remove build, java cup and jflex from orig.tar.gz.
  * Add cup and jflex as build dependency.
  * Patch weka source to use cup from debian.
  * Patch weka shell wrapper to use java-6-sun or openjdk.
  * Obtain documentation from svn.
  * Build depend on texlive-latex-extra (required to generate documentation).
  * Add javadoc as build target.
  * Use java-wrappers to start weka.

 -- Soeren Sonnenburg <sonne@debian.org>  Thu, 30 Oct 2008 06:42:46 +0100

weka (3.5.8-1) unstable; urgency=low

  [ Soeren Sonnenburg ]
  * Bump Standards Version to 3.8.0.
  * Remove references to non-free Java in debian/copyright.

  [ Torsten Werner ]
  * new upstream release
  * Switch to openjdk-6.
  * Move package to main.

 -- Torsten Werner <twerner@debian.org>  Sun, 10 Aug 2008 21:27:05 +0200

weka (3.5.7+tut1-1) unstable; urgency=low

  * Updated weka script to support calling other main classes,
    add jvm memory option and allow specific gui.
  * Update manpage for weka script, documenting the new options.
  * Fetch tutorial sources from public svn, generate them dynamically on build
    and include generated .pdfs in doc-base.

 -- Soeren Sonnenburg <debian@nn7.de>  Sun, 24 Feb 2008 09:18:45 +0100

weka (3.5.7-2) unstable; urgency=low

  * Change Architecture: all for the weka package.

 -- Torsten Werner <twerner@debian.org>  Wed, 19 Dec 2007 23:45:17 +0100

weka (3.5.7-1) unstable; urgency=low

  * Initial Release (Closes: #435930)

 -- Soeren Sonnenburg <debian@nn7.de>  Wed, 19 Dec 2007 19:50:51 +0100
