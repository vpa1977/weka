#!/bin/sh -e
set -x
# called by uscan with '--upstream-version' <version> <file>
SOURCE=$(dpkg-parsechangelog | sed -ne 's,Source: \(.*\),\1,p')
VERSION=$(dpkg-parsechangelog | sed -ne 's,^Version: \(.*\)-.*,\1,p')
WEKADASHVERSION=$( echo $2 | sed 's/\./-/g')
FILE=$3

DIR=weka-${WEKADASHVERSION}/
TAR=../${SOURCE}_${2}.orig.tar.gz

# clean up the upstream tarball
unzip $FILE
(cd $DIR && jar xf weka-src.jar)
(cd $DIR && \
  svn co https://svn.cms.waikato.ac.nz/svn/weka/tags/stable-$WEKADASHVERSION/wekadocs)
tar -czf $TAR --exclude '*.pdf' --exclude '*.jar' --exclude '*/wekadocs/changelogs' \
  --exclude '*/build/*' --exclude '*/doc/*' --exclude '*/wekadocs/data' \
  --exclude '*/weka/core/parser/JFlex' --exclude '*.svn*' \
  --exclude '*/weka/core/parser/java_cup' $DIR
rm -rf $DIR $FILE

# move to directory 'tarballs'
if [ -r .svn/deb-layout ]; then
  . .svn/deb-layout
  mv $TAR $origDir
  echo "moved $TAR to $origDir"
fi

exit 0
